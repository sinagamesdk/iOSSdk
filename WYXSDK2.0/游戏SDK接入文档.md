# 游戏SDK接入文档

## 1. 准备工作

### 与微游戏确认使用的Bundle Identifier，从微游戏处获取相关参数

* 游戏appkey：游戏的唯一标示
* 第三方授权参数
  * 微博
    * weiboAppid：微博授权appid
  * 微信
    * wechatAppid：微信授权appid
    * universalLink：微信授权universalLink
  * 热云
    * reyunAppid：热云appid
  * 闪验
    * shyanAppid：闪验appid
* 付款的产品ID与金额

### 2.搭建开发环境

#### 2.1 添加必要文件

1. 解压从微游戏获取的解压包，把所有文件添加到工程中
2. TARGETS-->Build Phases-->Link Binary With Libraries-->+ -->选择Security.framework、CoreTelephony.framework、AdSupport.framework、libsqlite3.tbd、iAd.framework 、AVFoundation.framework、libz.tbd、libresolv.tbd、libc++.tbd,CFNetwork.framework、WebKit.framework库文件。

#### 2.2 配置 other link

在`TARGETS`-->工程名-->`Build Settinds`--> 搜索 `other link`，添加`-ObjC`项

![](http://img.tysye.vip/1591066293.png?imageMogr2/thumbnail/!100p)



#### 2.3 添加URL Types

在`TARGETS`-->工程名-->`info`-->URL Types项下添加URL Schemes

注意：微博scheme为wb+微博的appid，微信的scheme为微信appid

![](http://img.tysye.vip/1590047237.png?imageMogr2/thumbnail/!100p)

#### 2.4 配置info.plist 文件

在`info.plist`文件中添加HTTP的支持

![](http://img.tysye.vip/1590046142.png?imageMogr2/thumbnail/!100p)



在`info.plist`文件中添加Scheme白名单,再加入myqqwpa与tim两项

![](http://img.tysye.vip/1590046339.png?imageMogr2/thumbnail/!100p)

在 info.plist 文件中添加关于申请IDFA的描述

![image-20210204100309965](http://img.tysye.vip//20210204100315.png?imageMogr2/thumbnail/!100p)

#### 2.5 配置允许内购与苹果登录项

在`TARGETS`-->工程名-->`Signing&Capabilities`-->`+Capability`添加内购与Apple登录项

![](http://img.tysye.vip/1590050008.png?imageMogr2/thumbnail/!100p)



#### 2.6 配置Universal Links

在`TARGETS`-->工程名-->`Signing&Capabilities`-->`+Capability`添加`Associated Domains`项并将从微游戏获取到的universalLink配置到这里

![](http://img.tysye.vip/1591164651.png?imageMogr2/thumbnail/!100p)



#### 2.7 第三方授权一些特殊配置

* 检测`AppDelegate.h`中是否有`@property (strong, nonatomic) UIWindow *window;`,没有则添加

* 如果你的工程通过*SceneDelegate*来进行生命周期管理，查看是否存在`SceneDelegate.h/m`文件，会造成一些授权问题

1. 删除掉`info.plist`中`Application Scene Manifest`选项
2. 同时，删除文件`SceneDelegate.h/m`
3. 移除`AppDelegate.m`中的SceneDelgate相关代码

![](http://img.tysye.vip/1591151670.png?imageMogr2/thumbnail/!100p)

![](http://img.tysye.vip/1591151691.png?imageMogr2/thumbnail/!100p)

## API接入说明



#### 1. 注册SDK

```objective-c
[MGApi registerApp:@"1234567" weiboApp:@"32962093" wechatAppid:@"wx5acb484fac2f1e" wechatUniversalLink:@"https://update.qp.games.weibo.com/apple-app-site-association/" shanyanAppid:@"2kvs1H" reyunAppid:@"3f10451407af8f85281be265291ee5" delegate:self];
```



**实现相关代理函数**

```objective-c
@interface AppDelegate ()<MicroGameSDKDelegate>
  
- (void)mg_logout:(NSString *)uid {
    // 用户退出,切换账号成功后会调用原账号的退出
}

/**
userInfo字段说明:
@uid:用户的uid
@access_token:用户token
@age:用户年龄
@is_adult:是否成人
@is_realname:是否实名
*/
- (void)mg_login:(NSDictionary *)userInfo error:(MGAuthErrCode )err {
    if(err == MGSDKPay_Err_Ok) {
      // 登陆成功
    }else {
      // 登录失败
    }
}

- (void)mg_oldOrderVerficationSuccess:(NSString *)orderid cpext:(NSString*)cpext{
    // 旧订单校验通过（下单付款的时候因为某些原因造成没有校验通过，后续重新校验的时候通过了）
}
```



**实现授权回调处理函数**

```objective-c
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return  [MGApi handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [MGApi handleOpenURL:url];
}


- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity    restorationHandler:(void(^)(NSArray<id<UIUserActivityRestoring>> * __nullable restorableObjects))restorationHandler {
        return [MGApi handleOpenUniversalLink:userActivity];
}
```



#### 2. 登录

```objective-c
 [MGApi login];
```

登录回调在初始化SDK给的代理回调中

#### 3. 联系客服

```objc
[MGApi contactQQ];
```



#### 4. 用户中心

```objective-c
[MGApi userCenter];
```

#### 5. 支付

```objective-c
 [MGApi paymentWithProductid:@"Evaoverseas001" price:@"600" orderid:@"123" cpext:@"cpex_600" callback:^(NSString *orderid, NSString *cpext, MGPayErrCode error) {
           if(error == MGSDKPay_Err_Ok) {
             // 支付成功
           }else {
            //  支付失败
           }
    }];
```

价格单位注意是分，比如产品是18元，则传递`@“1800”`;

#### 6. 极光接入

说明: 与deviceToken进行绑定的别名为用户UID

##### 6.1 添加推送功能

![image-20210727155751163](http://img.tysye.vip//20210727155751.png?imageMogr2/thumbnail/!100p)

##### 6.2 初始化推送(AppDelegate 文件中)



```objective-c
导入头文件
  #import "JPUSHService.h"
// iOS10 注册 APNs 所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

// 在相关系统方法中进行处理
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   ...
   ...
    [self configJPush:launchOptions];
    return YES;
}

- (void)configJPush:(NSDictionary *)launchOptions {
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound|JPAuthorizationOptionProvidesAppNotificationSettings;

    // 通知回调
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    
    [JPUSHService setupWithOption:launchOptions appKey:@"84124053c437720666ed4ece"
                          channel:@"AppStore"
                 apsForProduction:YES
            advertisingIdentifier:nil];
    
}

```

##### 6.3 注册DeviceToken (AppDelegate 文件中)

```objective-c
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [MGApi registerDeviceToken:deviceToken];
}
```

##### 6.4 处理回调

```objective-c
#pragma mark- JPUSHRegisterDelegate

// iOS 12 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center openSettingsForNotification:(UNNotification *)notification{
  if (notification && [notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
    //从通知界面直接进入应用
  }else{
    //从通知设置界面进入应用
  }
}

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
  // Required
  NSDictionary * userInfo = notification.request.content.userInfo;
  if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
    [JPUSHService handleRemoteNotification:userInfo];
  }
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [JPUSHService setBadge:0];
  completionHandler(UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有 Badge、Sound、Alert 三种类型可以选择设置
}

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
  // Required
  NSDictionary * userInfo = response.notification.request.content.userInfo;
  if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
    [JPUSHService handleRemoteNotification:userInfo];
  }
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [JPUSHService setBadge:0];
  completionHandler();  // 系统要求执行这个方法
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {

  // Required, iOS 7 Support
  [JPUSHService handleRemoteNotification:userInfo];
  completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {

  // Required, For systems with less than or equal to iOS 6
  [JPUSHService handleRemoteNotification:userInfo];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [application setApplicationIconBadgeNumber:0];
    [application cancelAllLocalNotifications];
}
```

