//
//  MGameViewController.m
//  MGUnionSDK
//
//  Created by Mr.Song on 2020/6/3.
//  Copyright © 2020 Mr.Song. All rights reserved.
//

#import "MGameViewController.h"
#import "MGAPi.h"


@interface MGameViewController ()

@end

@implementation MGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"游戏界面";
    self.navigationItem.hidesBackButton = YES;
    // Do any additional setup after loading the view from its nib.
}


- (IBAction)payment:(id)sender {
    [MGApi paymentWithProductid:@"Evaoverseas001" price:@"600" orderid:@"123" cpext:@"cpex6" callback:^(NSString *orderNum, NSString *cpext, MGPayErrCode error) {
        if(error == MGSDKPay_Err_Ok) {
          // 支付成功
        }else {
         //  支付失败
        }
    }];
    
}
- (IBAction)payment2:(id)sender {
    [MGApi paymentWithProductid:@"Evaoverseas003" price:@"5000" orderid:@"2345" cpext:@"cpex50" callback:^(NSString *orderNum, NSString *cpext, MGPayErrCode error) {
        if(error == MGSDKPay_Err_Ok) {
          // 支付成功
        }else {
         //  支付失败
        }
    }];
}
- (IBAction)payment3:(id)sender {
    [MGApi paymentWithProductid:@"Evaoverseas005" price:@"168000" orderid:@"4567" cpext:@"cpex168" callback:^(NSString *orderNum, NSString *cpext, MGPayErrCode error) {
        if(error == MGSDKPay_Err_Ok) {
           // 支付成功
        }else {
         //  支付失败
        }
    }];
}

- (IBAction)internalTest:(id)sender {
    [MGApi testInternalFunc];
}


- (IBAction)personCenter:(id)sender {
    [MGApi userCenter];
}

@end
