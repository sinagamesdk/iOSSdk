//
//  AppDelegate.h
//  UnionSDKDemo
//
//  Created by Mr.Song on 2020/7/13.
//  Copyright © 2020 Mr.Song. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MGGViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

//
@property(nonatomic, weak)MGGViewController *mgDelegate;

@end

