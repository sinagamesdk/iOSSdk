//
//  AppDelegate.m
//  UnionSDKDemo
//
//  Created by Mr.Song on 2020/7/13.
//  Copyright © 2020 Mr.Song. All rights reserved.
//

#import "AppDelegate.h"
#import "MGApi.h"
#import "MGGViewController.h"

@interface AppDelegate ()<MicroGameSDKDelegate>

@end

@implementation AppDelegate


- (void)mg_logout:(NSString *)uid {
    // 用户退出
}

// 用户登陆成功后进入游戏主界面
- (void)mg_login:(NSDictionary *)userInfo error:(MGAuthErrCode )err {
    if(err == MGSDKPay_Err_Ok) {
      // 登陆成功
        [self.mgDelegate noticeLoginResult:YES];
    } else {
        [self.mgDelegate noticeLoginResult:NO];
    }
}

- (void)mg_oldOrderVerficationSuccess:(NSString *)orderid cpext:(NSString *)cpext {
    // 旧订单校验通过
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
 //   [MGApi enableDebugMode:YES];
    [MGApi registerApp:@"2368257394" weiboApp:@"2368257394" wechatAppid:@"wx5acbcc484fac2f1e" wechatUniversalLink:@"https://sng.weiyouxi.com"
        shanyanAppid:@"2kvsYH1H" reyunAppid:@"3f10451407a93f8f85281be265291ee5"delegate:self];
    // Override point for customization after application launch.
    return YES;
}

- (BOOL)application:(UIApplication *)app
openURL:(NSURL *)url
            options:(NSDictionary<NSString *,id> *)options {
    return  [MGApi handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return  [MGApi handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [MGApi handleOpenURL:url];
}


- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity    restorationHandler:(void(^)(NSArray<id<UIUserActivityRestoring>> * __nullable restorableObjects))restorationHandler {
        return [MGApi handleOpenUniversalLink:userActivity];
}




@end
