//
//  MGViewController.m
//  MGUnionSDK
//
//  Created by StartEnd on 03/31/2020.
//  Copyright (c) 2020 StartEnd. All rights reserved.
//

#import "MGGViewController.h"
#import "MGApi.h"
#import "MGameViewController.h"
#import "AppDelegate.h"

@interface MGGViewController ()

@end

@implementation MGGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.mgDelegate = self;
    [MGApi login];
}

- (void)noticeLoginResult:(BOOL)bl {
    if(bl) {
        MGameViewController *vc = [[MGameViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        //登录失败
    }
}

- (IBAction)login:(id)sender {
    [MGApi login];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
