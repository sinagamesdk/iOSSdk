//
//  MGViewController.h
//  MGUnionSDK
//
//  Created by StartEnd on 03/31/2020.
//  Copyright (c) 2020 StartEnd. All rights reserved.
//

@import UIKit;

@interface MGGViewController : UIViewController

- (void)noticeLoginResult:(BOOL)bl;

@end
