//
//  SNGUnionSDK.h
//  SNGUnionSDK
//
//  Created by xu huijun on 15/1/16.
//  Copyright (c) 2015年 xhj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WBHttpRequest.h"
#import "AFHTTPSessionManager.h"

@interface SNGUnionSDK : NSObject

+(SNGUnionSDK *)SNGUnionSDKShared;

@property(assign) NSString *gh_token;
@property(assign) NSString *gh_uid;
@property(assign) NSString *suid;
@property(assign) NSString *token;
@property(assign) NSString *appkey;
@property(assign) NSString *accountName;
@property(assign) NSString *appid;
@property(assign) NSString *redirectURI;

@property(nonatomic, assign)BOOL shouldUseStandbyUrl;


+ (void)sngunion_Delete:(NSString *)URLString
             parameters:(id)parameters
                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

#pragma mark -  旧版接口（暂时没用）
/***************************************************************************************/
/*                                         旧版接口（暂时没用）                            */
/***************************************************************************************/

/*
 * 设备激活回调接口
 *
 * uid 用户的编号
 * appkey 当前游戏的key
 * appleid 应用的AppStore ID
 */

+ (void)activeCallbackWithUserId:(NSString *)uid
                          appkey:(NSString *)appkey
                         appleid:(NSString *)appleid
           withCompletionHandler:(WBRequestHandler)handler;


/*
 * 支付回调接口
 *
 * orderid 订单的唯一编号
 * uid 用户的编号
 * appkey 当前游戏的key
 * appleid 应用的AppStore ID
 * amount 付费金额，单位分
 */

+ (void)payCallbackWithOrderid:(NSString *)orderid
                        UserId:(NSString *)uid
                        appkey:(NSString *)appkey
                       appleid:(NSString *)appleid
                        amount:(NSString *)amount
         withCompletionHandler:(WBRequestHandler)handler;





/*
 * 设备激活接口
 *
 * uid 用户的编号
 * appkey 当前游戏的key
 * appleid 应用的AppStore ID
 */

+ (void)activeWithUserId:(NSString *)uid
                  appkey:(NSString *)appkey
                 appleid:(NSString *)appleid
                 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


NSString * dLogPwdWithKey(NSString *str);

#pragma mark -  用户接口
/***************************************************************************************/
/*                                         用户接口                                     */
/***************************************************************************************/

/*
 *  用户登录校验接口
 *
 * appkey 当前游戏的key
 * deviceid 设备id
 * suid 用户的编号
 * token 用户身份标识
 * gh_uid 公会用户id
 * gh_token 公会系统token
 * signature 签名串
 */

+ (void)userCheckinSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 *  新浪账号注册接口
 *
 * appkey 当前游戏的key
 * deviceid 设备id
 * uid 新浪用户的编号
 * token 用户身份标识
 * machineid 加密后的设备id，新浪通行证token时传递
 * machineidbase64 Base64后的设备id，新浪通行证token时传递
 * signature 签名串
 */

+ (void)userRegSinaWithUserID:(NSString *)uid
                        token:(NSString *)token
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (void)userRegSinaWithSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 *  游客注册接口
 *
 * appkey 当前游戏的key
 * deviceid 设备id
 * signature 签名串
 */


+ (void)userRegDeviceSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 *  手机一键注册接口
 *
 * appkey 当前游戏的key
 * deviceid 设备id
 * vcode 手机验证码
 * signature 签名串
 */

+ (void)userRegAutoWithVcode:(NSString *)vcode
                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 *  请求手机验证码接口
 *
 * appkey 当前游戏的key
 * deviceid 设备id
 * mobile 手机号
 * gh_uid 17g用户的编号
 * gh_token 17g用户身份标识
 */

+ (void)userRegGetVcodeWithMobile:(NSString *)mobile
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 *  下行手机号注册接口
 *
 * appkey 当前游戏的key
 * deviceid 设备id
 * mobile 手机号
 * vcode 手机验证码
 * signature 签名串
 */

+ (void)userRegManuWithMobile:(NSString *)mobile
                        vcode:(NSString *)vcode
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 *  重置密码接口
 *
 * appkey 当前游戏的key
 * deviceid 设备id
 * mobile 手机号
 * vcode 手机验证码
 * pwd 密码
 * signature 签名串
 */

+ (void)userResetWithMobile:(NSString *)mobile
                      vcode:(NSString *)vcode
                        pwd:(NSString *)pwd
                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 *  手机号登录接口
 *
 * appkey 当前游戏的key
 * deviceid 设备id
 * mobile 手机号
 * pwd 密码
 * signature 签名串
 */


+ (void)userLoginWithMobile:(NSString *)mobile
                        pwd:(NSString *)pwd
                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 *  公会用户登录接口
 *
 * appkey 当前游戏的key
 * deviceid 设备id
 * account 用户名，邮箱或者手机号
 * pwd 密码
 * signature 签名串
 */

+ (void)userLoginGHWithAccount:(NSString *)account
                           pwd:(NSString *)pwd
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 *  新浪账号绑定接口
 *
 * appkey 当前游戏的key
 * deviceid 设备id
 * suid 用户的编号
 * uid 用户新浪账号的id
 * token 用户身份标识
 * machineid 加密后的设备id，新浪通行证token时传递
 * machineidbase64 Base64后的设备id，新浪通行证token时传
 * gh_uid 公会用户id
 * gh_token 公会系统token
 * signature 签名串
 */

+ (void)userBindSinaWithSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 *  手机号绑定接口
 *
 * appkey 当前游戏的key
 * deviceid 设备id
 * suid 用户的编号
 * mobile 手机号
 * vcode 短信验证码
 * gh_uid 公会用户id
 * gh_token 公会系统token
 * signature 签名串
 */

+ (void)userBindMobileWithMobile:(NSString *)mobile
                           vcode:(NSString *)vcode
                        password:(NSString *)password
                         success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 *  检查用户手机号是否已注册接口
 *
 * appkey 当前游戏的key
 * mobile 手机号
 * signature 签名串
 */

+ (void)userCheckMobileWithMobile:(NSString *)mobile
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/**
 *  17g用户绑定接口
 *
 *  @param account  账号名
 *  @param password 密码
 *  @param success
 *  @param failure
 */
+ (void)userBindWithAccount:(NSString *)account password:(NSString *)password
                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/**
 *  17g用户注册接口
 *
 *  @param account  用户名
 *  @param pwd     密码
 *  @param success
 *  @param failure
 */
+ (void)userReg17gWithAccount:(NSString *)account pwd:(NSString *)pwd
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

#pragma mark - 游戏其他接口
/***************************************************************************************/
/*                                         游戏其他接口                                  */
/***************************************************************************************/

/*
 * 实名制认证
 */
+ (void)userCertificationWithRealName:(NSString *)realName cardNum:(NSString *)cardNum
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 *  游戏手机号绑定
 *
 * appkey 当前游戏的key
 * mobile 手机号
 * signature 签名串
 */

+ (void)yxBindWithMobile:(NSString *)mobile
                   vcode:(NSString *)vcode
                 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
/*
 * 游戏自己调用（应该由服务端之间验证，安全）
 */

+ (void)yxShowGameMobileSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * 新浪游戏用户协议接口
 * 新增20171031
 */

+ (void)userAgreementWithSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

#pragma mark - 支付接口
/***************************************************************************************/
/*                                         支付接口                                     */
/***************************************************************************************/
/*
 *  支付下单接口(20170322)
 *
 * source 当前游戏的key
 * method 固定写死为order
 * suid 当前登录用户的id
 * pid  购买商品id
 * subject 商品标题
 * amount 总金额（单位分）
 * signature 签名串
 * pt false 透传参数该参数会在回调通知时原样返回，若不需要此参数请不要传该参数，且其值不能为空格或NULL，在查询订单接口时如填写了pt则会返回pt参数
 * access_token 采用OAuth授权方式为必填参数，由授权后获得
 * vcode 当前微博的版本号
 */

+ (void)paymentOrderWithSource:(NSString *)source
                        method:(NSString *)method
                           pid:(NSString *)pid
                         appid:(NSString *)appid
                       subject:(NSString *)subject
                        amount:(int)amount
                            pt:(NSString *)pt
                  access_token:(NSString *)access_token
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/**
 支付校验接口(20170322)
 
 @param payType 支付类型
 @param data
 @param success
 @param failure
 */

+ (void)paymentCheckWithPayType:(NSString *)payType
                           data:(NSString *)data
                       orderNum:(NSString *)orderNum
                          appid:(NSString *)appid
                            pid:(NSString *)pid
                   transacionid:(NSString *)transacionid
                        success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 *  更新订单状态接口
 *
 * order_id 当前订单
 * access_token 采用OAuth授权方式为必填参数，由授权后获得
 * paytype 支付业务号
 */

+ (void)paymentUpdateWithAccessToken:(NSString *)accessToken
                             orderId:(NSString *)orderId
                             payType:(int)payType
                             success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                             failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 *  查询订单状态接口
 *
 * order_id 当前订单
 * access_token 采用OAuth授权方式为必填参数，由授权后获得
 * paytype 支付业务号
 */

+ (void)paymentQueryWithAccessToken:(NSString *)accessToken
                            orderId:(NSString *)orderId
                            payType:(int)payType
                            success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


#pragma mark -  系统接口
/***************************************************************************************/
/*                                         系统接口                                     */
/***************************************************************************************/


/*
 *  获取系统配置接口
 *
 * appkey 当前游戏的key
 * deviceid 设备id
 * suid 用户唯一编号
 * token 用户身份标识
 * signature 签名串
 */

+ (void)appShowNotificationSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                           failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 *  检查更新接口
 *
 * appkey 当前游戏的key
 * channelid 渠道id
 * version_code 版本号
 * package_name 包名
 */

+ (void)appVersionWithChannelid:(NSString *)channelid
                   version_code:(NSString *)version_code
                   package_name:(NSString *)package_name
                        success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/**
 日志接口
 每次打开客户端时调用，将客户端基础信息传给服务器
 
 @param action 日志类型（首次激活 active，打开  open，登录  login）
 @param uid 登录成功的用户需要传，还未授权的用户传0
 @param appkey 游戏的key，用于区分不同的游戏
 @param rom 当前设备系统的版本
 @param ua 设备型号
 @param app_versioncode 客户端版本号
 @param app_versionname 客户端版本名称
 @param package 游戏包名，ios为bundle id
 @param channelid 客户端渠道号，用于区分不同的安装包
 @param net_info 网络类型 ，2g,3g,4g wifi，等
 
 */

+ (void)reportLogWithAction:(NSString *)action
            app_versioncode:(int)app_versioncode
            app_versionname:(NSString *)app_versionname
                    package:(NSString *)package
                  channelid:(NSString *)channelid
                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 *  检查sdk更新接口
 *
 * appkey 当前游戏的key
 * sdk_version sdk版本号
 */

+ (void)appVersionSdk_version:(NSString *)sdk_version
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * 错误记录接口
 *
 * appkey 当前游戏的key
 * package  游戏包名
 * suid  用户id
 * channelid  渠道号
 */

+ (void)appVersionWithAppkey:(NSString *)appkey
                     package:(NSString *)package
                   channelid:(NSString *)channelid
                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 * 游戏兑换码-兑换接口
 *
 * appkey 当前游戏的key
 * channel  渠道号
 * code  兑换码的内容
 * uid  uid，与设备号必传其一
 * deviceid  设备号，与uid必传其一
 * signature  签名串
 */


+ (void)appVersionWithChannel:(NSString *)channel
                         code:(NSString *)code
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;



#pragma mark - 新浪账号登录接口
/***************************************************************************************/
/*                                         新浪账号登录接口    （邮箱）                    */
/***************************************************************************************/

/*
 * 预登入接口
 *
 * user 用户名称
 */


+ (void)sina_preloginWithUserName:(NSString *)user
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 * 验证码接口文档
 *
 * pcid currentPcid
 */

+ (void)sina_captchaWithCurrentPcid:(NSString *)pcid
                            success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * 新浪账号登入
 *
 * door 用户输入验证码
 */

+ (void)sina_loginWithDoor:(NSString *)door
               currentPcid:(NSString *)pcid
                  userName:(NSString *)user
              userPassword:(NSString *)password
                   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 * 新浪账号绑定
 *
 * door 用户输入验证码
 */

+ (void)sina_bindWithDoor:(NSString *)door
               currentPcid:(NSString *)pcid
                  userName:(NSString *)user
              userPassword:(NSString *)password
                   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


#pragma mark - 微信账号登入
/***************************************************************************************/
/*                                         微信账号登录接口                               */
/***************************************************************************************/
/*
 * 微信账号登入（获取accesstoken）
 * appid
 * secret
 * code
 */

+ (void)wx_getAccessTokenWithAppid:(NSString *)appid
                           secret:(NSString *)secret
                             code:(NSString *)code
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 * 微信账号登入(通过refreshtoken 获取新的 accesstoken 或者延长过期时间）
 * appid
 * refresh_token
 */

+ (void)wx_refreshTokenWithAppid:(NSString *)appid
                   refresh_token:(NSString *)refresh_token
                         success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 * 微信账号登入（获取用户信息）
 * appid
 * secret
 * code
 */
+ (void)wx_getUserinfoWithAccessToken:(NSString *)token
                               openid:(NSString *)openid
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*
 * 微信账号注册
 * appid
 * secret
 * code
 */
+ (void)wx_userRegWithAccessToken:(NSString *)token
                          unionid:(NSString *)unionid
                           openid:(NSString *)openid
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;



@end
