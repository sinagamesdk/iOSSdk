//
//  SNGUnionUtil.h
//  SNGUnionSDK
//
//  Created by xu huijun on 15/4/17.
//  Copyright (c) 2015年 xhj. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum
{
    SNGUserTypeWeibo = 99,
    SNGUserTypeFastPlay = 1,
    SNGUserTypeOther = 2,
    SNGUserType17G = 10,
    SNGUserTypeWechat = 12,
    
}SNGUserType;

@interface SNGUnionUtil : NSObject

typedef void (^SNGUnionUtilSuccessBlock)(id params);
typedef void (^SNGUnionUtilFailureBlock)(id params, NSError *error);

/**
 *  登录(微博登录，快速登录，其他登录)
 *  @param reloadView 二次登录时是否展示登录框，YES：展示，NO：不展示
 *  @param success 登录成功返回
 *  @param failure 登录失败返回
 *  @param bindsuccess 绑定成功返回
 *  @param bindfailure 绑定失败返回
 *  @ 当用户为游客模式，在loading页展示绑定入口
 */

+ (void)sng_LoginWithReloadView:(BOOL)reloadView
                        success:(void (^)(id params))success
                        failure:(void (^)(id params, NSError *error))failure
                    bindsuccess:(void (^)(id params))bindsuccess
                    bindfailure:(void (^)(id params, NSError *error))bindfailure;

/**
 *  登录(微博登录，微信登录，快速登录，其他登录)
 *
 *  @param theViewController  当前界面对象
 *  @param reloadView 二次登录时是否展示登录框，YES：展示，NO：不展示
 *  @param success 登录成功返回
 *  @param failure 登录失败返回
 *  @param bindsuccess 成功返回
 *  @param bindfailure 失败返回
 *  @ 当用户为游客模式，在loading页展示绑定入口
 */

+ (void)sng_LoginInViewController:(UIViewController *)theViewController
                       reloadView:(BOOL)reloadView
                          success:(void (^)(id params))success
                          failure:(void (^)(id params, NSError *error))failure
                      bindsuccess:(void (^)(id params))bindsuccess
                      bindfailure:(void (^)(id params, NSError *error))bindfailure;

/**
 *  新浪账号登录
 */
+ (void)sng_RegSina;

/**
 *  绑定新浪账号
 */
+(void)sng_bindSina;

/**
 *  退出
 *
 *  @param path 退出时展示图片
 */
+ (void)sng_QuitGame:(NSString *)path;

/**
 *  注销
 *
 *  @return 注销成功返回YES，注销失败返回NO。
 */
+ (BOOL)sng_LogOut;

/**
 *  判断是否已经登录
 *
 *  @return 已登录返回YES，未登录返回NO。
 */
+ (BOOL)sng_IsLogin;

///**
// *  展示webview
// *
// *  @param path 地址
// */
//+ (void)showWebViewWithUrl:(NSString *)path  completionHandler:(void (^)(void))completionHandler;
//
///**
// *  展示webview
// *
// *  @param path 地址
//
// */
//+ (void)showBrowserWithUrl:(NSString *)path holdCompletionHandler:(void (^)(id params))holdCompletionHandler;
//
///**
// * webview展示论坛
// *
// * @param level 级别
// * @param gameCurrency 金币
// * @param winP 胜率
// * @param accessKey 平台接入key 必填
// * @param secretKey 平台接入secretKey 必填
// * @param completionHandler 关闭返回
// */
//+ (void)showForumWithShortName:(NSString *)shortName
//                         level:(NSString *)level
//                  gameCurrency:(NSString *)gameCurrency
//                          winP:(NSString *)winP
//                     accessKey:(NSString *)accessKey
//                     secretKey:(NSString *)secretKey
//             completionHandler:(void (^)(void))completionHandler;

/**
 *  游客绑定
 *
 *  @param success 成功返回
 *  @param failure 失败返回
 */
+ (void)sng_BindSuccess:(void (^)(id params))success
                failure:(void (^)(id params, NSError *error))failure;

/**
 *  游戏福利绑定
 *
 *  @param success 成功返回
 *  @param failure 失败返回
 */
+ (void)sng_yxBindSuccess:(void (^)(id params))success
                  failure:(void (^)(id params, NSError *error))failure;

/**
 *  判断是否是游客登录
 *
 *  @return 是游客返回YES，不是游客返回NO。
 */
+ (BOOL)sng_IsTourist;


/*! @brief 检查微信是否已被用户安装
 *
 *  @return 微信已安装返回YES，未安装返回NO。
 */
+ (BOOL)isWXAppInstalled;

/**
 获取登录名

 @param usertype 用户类型
 @return 返回当前登录用户名
 */
+ (NSString *)getWelcomName:(SNGUserType)usertype;

/**
 查看用户协议
 */
+ (void)sng_showUserAgreement;


@end
