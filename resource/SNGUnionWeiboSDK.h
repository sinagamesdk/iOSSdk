//
//  SNGUnionWeiboSDK.h
//  SNGUnionSDK
//
//  Created by xu huijun on 15/1/16.
//  Copyright (c) 2015年 xhj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeiboSDK.h"


/**
 接收并处理来至微博客户端程序的事件消息
 */
@protocol SNGUnionWeiboSDKDelegate <NSObject>

/**
 收到一个来自微博客户端程序的请求
 
 收到微博的请求后，第三方应用应该按照请求类型进行处理，处理完后必须通过 [WeiboSDK sendResponse:] 将结果回传给微博
 @param request 具体的请求对象
 */
- (void)didReceiveWeiboRequest:(WBBaseRequest *)request;

/**
 收到一个来自微博客户端程序的响应
 
 收到微博的响应后，第三方应用可以通过响应类型、响应的数据和 WBBaseResponse.userInfo 中的数据完成自己的功能
 @param response 具体的响应对象
 */
- (void)didReceiveWeiboResponse:(WBBaseResponse *)response;

@end

@interface SNGUnionWeiboSDK : WeiboSDK

+(SNGUnionWeiboSDK *)shared;

@property (nonatomic, retain) NSString *userID;

@property (nonatomic,retain)NSString *accessToken;


/**
 向微博客户端程序注册第三方应用
 @param appKey 微博开放平台第三方应用appKey
 @param appId    应用ID（itunes connect）
 @param redirectURI    应用回调地址
 @return 注册成功返回YES，失败返回NO
 */
+ (BOOL)registerApp:(NSString *)appKey
              appid:(NSString *)appid
        redirectURI:(NSString *)redirectURI;

/**
 处理微博客户端程序通过URL启动第三方应用时传递的数据
 
 需要在 application:openURL:sourceApplication:annotation:或者application:handleOpenURL中调用
 @param url 启动第三方应用的URL
 @param delegate WeiboSDKDelegate对象，用于接收微博触发的消息
 @see WeiboSDKDelegate
 */
+ (BOOL)handleOpenURL:(NSURL *)url delegate:(id<SNGUnionWeiboSDKDelegate>)delegate;


/**
 设置WeiboSDK的调试模式
 
 当开启调试模式时，WeiboSDK会在控制台输出详细的日志信息，开发者可以据此调试自己的程序。默认为 NO
 @param enabled 开启或关闭WeiboSDK的调试模式
 */
+ (void)enableDebugMode:(BOOL)enabled;

/*
 * 授权接口
 */

+ (void)loginWithAppkey:(NSString *)appkey
                  appid:(NSString *)appid
            redirectURI:(NSString *)redirectURI
               userinfo:(NSString *)userInfo;


/*
 * 取消授权接口
 */

+ (void)loginOut;


@end
