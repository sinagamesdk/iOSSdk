//
//  SNGPayManager.h
//  SNGUnionSDK
//
//  Created by xu huijun on 2017/3/7.
//  Copyright © 2017年 xhj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNGPayManager : NSObject

typedef void (^SNGPayManagerSuccessBlock)(NSString * orderid,BOOL iap,NSString *message);
typedef void (^SNGPayManagerFetchProductBlock)();
typedef void (^SNGPayManagerFailureBlock)(NSString * orderid,BOOL iap,NSString *message);


+ (SNGPayManager *)sharedInstance;

-(void)addTransactionObserverAndVerify:(BOOL)verify;

-(void)removeTransactionObserver;

/**
 *  购买一个商品
 *
 *  @param productId  商品id
 *  @param appid  苹果自动为内购生成的id
 *  @param subject  标题
 *  @param amount  总金额（单位分）
 *  @param pt  透传参数
 */

- (void)sng_buyWithPid:(NSString *)productId
                 appid:(NSString *)appid
               subject:(NSString *)subject
                amount:(int)amount
                    pt:(NSString *)pt
               success:(SNGPayManagerSuccessBlock)success
          fetchProduct:(SNGPayManagerFetchProductBlock)fetchProduct
               failure:(SNGPayManagerFailureBlock)failure;

/**
 *  恢复购买
 */
- (void)sng_restore;

/**
 *  重新验证未验证成功的收据
 */

- (void)sng_verifyReceipt;

- (void)sng_verifyReceiptWithData:(NSString *)data
                         orderNum:(NSString *)orderNum
                            appid:(NSString *)appid
                              pid:(NSString *)pid
                     transacionid:(NSString *)transacionid;

/**
 *  本地化标题
 */
- (NSString *)sng_titleMatchingProductIdentifier:(NSString *)identifier;

@end
