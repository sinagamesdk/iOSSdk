//
//  SNGWXManager.h
//  SNGUnionSDK
//
//  Created by xu huijun on 16/4/5.
//  Copyright © 2016年 xhj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"

@protocol SNGWXManagerDelegate <NSObject>

@optional

- (void)managerDidRecvAuthSuccess;
- (void)managerDidRecvAuthFailure:(id)failure;

@end

@interface SNGWXManager : NSObject<WXApiDelegate>

@property(assign,readonly) NSString *appid;
@property(assign,readonly) NSString *secret;
@property(assign,readonly) NSString *scope;
@property(assign,readonly) NSString *state;
@property(assign,readonly) NSString *openID;

@property (nonatomic,retain) NSString *openid;
@property (nonatomic,retain) NSString *accessToken;
@property (nonatomic,retain) NSString *unionid;

@property (nonatomic, assign) id<SNGWXManagerDelegate> delegate;

+ (instancetype)sharedManager;

/*! @brief SNGWXManager的成员函数，向微信终端程序注册第三方应用。
 *
 * 需要在每次启动第三方应用程序时调用。第一次调用后，会在微信的可用应用列表中出现。
 * @see registerApp
 * @param appid 微信开发者ID
 * @param appdesc 应用附加信息，长度不超过1024字节
 * @return 成功返回YES，失败返回NO。
 */

- (BOOL)wx_registerApp:(NSString *)appid
                secret:(NSString *)secret
                 scope:(NSString *)scope
                 state:(NSString *)state
       withDescription:(NSString *)appdesc;

- (void)wx_sendAuthRequestInViewController:(UIViewController *)theViewController;

- (void)wx_loginOut;

@end
