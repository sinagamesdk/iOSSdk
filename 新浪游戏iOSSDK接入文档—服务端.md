# SNG联运游戏平台接口文档ForCP(服务端)

## 1.用户接口
### 1.1.用户信息校验接口（服务端）
http://m.game.weibo.cn/api/sdk/user/check.json
用于用户身份校验。
#### 支持格式
JSON（返回为json，请求为标准http form的格式）
#### 接口登录密钥signature key，请联系新浪游戏运营人员索取
#### HTTP请求方式
POST
#### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
suid|string       | 用户的编号  | true
appkey|string       | 当前游戏的key  | true
deviceid|string       | 设备id  | true
token|string       | 用户身份标识 | true
signature|string       | 签名串  | true

#### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
suid	|string|
token         |string |
usertype|string	|账号类型:1 快速试玩 2手机 3新浪通行证 99微博

#### 返回示例
```json
{
"suid": "3508971941",
"token": "tXvasFVztJrB7ebc06600eFVEXdB", 
"usertype":1
}
```

####测试示例

----------

## 2.支付接口
### 2.1.回调接口由开发商提供
与上述各接口不同，回调接口由各家CP各自进行开发和部署，遵循一致的规范，供手机微游戏调用。
#### 接口加密密钥，请使用游戏的 appsecret
#### HTTP请求方式
GET
#### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
order_id|string       | 调用下单接口获取到的订单号 | true
amount|int      | 支付金额，单位 分  | true
order_uid|string       | 支付用户id  | true
source|string       | 应用的appkey | true
actual_amount|int      | 实际支付金额，单位 分 | true
pt|string|透传参数（该参数的有无决定于下单时有没有上传pt参数） | true
signature|string   |用于参数校验的签名，生成办法参考附录一| true

#### 返回参数说明
```
http状态码应为200，返回结果为字符串OK，且必须是大写。
```
####测试示例

----------
##3.附录

### 3.1.接口错误的返回格式
```json
{
	"request" : "/rank/show",
	"error_code" : "10013",
	"error" : "Invalid weibo user"
}
```
#### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
request	|string| 请求的url
error_code         |string |具体含义请参考http://open.weibo.com/wiki/Error_code
error|string	|错误的说明

### 3.2.用户信息接口签名机制
####1、签名规则
```
a)参数signature不参与签名
b)值为空的参数，也需要参与签名
c)若参数中包含特殊字符,例如中文,&,%,@等, 需要在签名之后进行url_encode
d)目前仅支持md5方式签名
```
####2、签名步骤
```
a)将所有待签名参数按参数名排序(字母字典顺序，例如PHP的ksort()函数)
b)把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串，组成字符串A
c)signature_key是新浪游戏分配的登录密钥，请联系新浪游戏运营人员索取
d)将字符串A与signature_key，用英文竖杠进行连接, 得到字符串C，对字符串C取md5值，得到字符串D，D就是所需要的签名
```
####3、示例
```
/**
 * 生成签名结果
 * @param $params 已排序要签名的数组
 * @param $sina_secret 新浪游戏运营人员分配的登录密钥
 * return 签名结果字符串
 */
public static function buildRequestMysign($params, $signature_key)
{
	if (!isset($params['appkey'])) return '';
	if (empty($signature_key)) return '';
	if (isset($params['signature'])) unset($params['signature']);
	//将所有待签名参数按参数名排序
	ksort($params);
	
	//把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串，组成字符串A
	$str_A = self::_createLinkstring($params);
	
	//将字符串A与signature_key，用英文竖杠进行连接, 得到字符串C
	$str_C = sprintf('%s|%s', $str_A, $signature_key);
	
	//对字符串C取md5值，得到字符串D，D就是所需要的签名
	$str_D = md5($str_C);
	
	return $str_D;
}

/**
 * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
 * @param $para 需要拼接的数组
 * return 拼接完成以后的字符串
 */
private static function _createLinkstring($para)
{
	$arg = '';
	while (list ($key, $val) = each ($para)) {
		$arg .= $key.'='.$val.'&';
	}
	//去掉最后一个&字符
	$arg = substr($arg, 0, -1);
	
	//如果存在转义字符，那么去掉转义
	if(get_magic_quotes_gpc()){$arg = stripslashes($arg);}
	
	return $arg;
}
```
### 3.3.支付接口签名机制
####1、签名规则
```
a)参数signature不参与签名
b)用于计算时参数取值不要做urlencode。
c)根据pt参数的有无，决定是否加上pt参数
```
####2、签名步骤
```
a)将所有待签名参数按参数名排序(字母字典顺序，例如PHP的ksort()函数)
b)把数组所有元素，按照“参数|参数值”的模式用“|”字符拼接成字符串，组成字符串A
c)将字符串A与 appsecret，用英文竖杠进行连接, 得到字符串B，对字符串B取sha1值，得到字符串C，C就是所需要的签名
```
####3、示例
```
http://test.game.weibo.cn/paysys/pay.php?amount=1&desc=test&from=101010001000&method=order&pt=qwertyuiopasdfghjkl1234567890&sessionkey=sessionkey&source=3093918086&uid=1985490674&signature=6a158cafc6fbd3a94310196356c2b29c43abc975
则计算签名值的方法为：
sha1(“amount|1|desc|test|from|101010001000|method|order|pt|qwertyuiopasdfghjkl1234567890|sessionkey|sessionkey|source|3093918086|uid|1985490674|appsecret”)

/**
 * 生成签名结果
 * @param $para_sort 已排序要签名的数组
 * return 签名结果字符串
 */
public static function buildRequestMysign($secret)
{
	if (empty($_REQUEST)) return FALSE;
	if (isset($_REQUEST['signature'])) unset($_REQUEST['signature']);
	//将所有待签名参数按参数名排序
	ksort($_REQUEST);

	//把数组所有元素，按照“参数|参数值”的模式用“|”字符拼接成字符串，组成字符串A
    $str_A = '';
    foreach ($_REQUEST as $key => $value)
    {
        $str_A .= sprintf('%s|%s|', $key, $value);
    }

	//将字符串A与appsecret，用英文竖杠进行连接, 得到字符串B
	$str_B = $str_A . $secret;

	//对字符串B取sha1值，得到字符串C，C就是所需要的签名
	$str_C = sha1($str_B);

	return $str_C;
}
```

错误码 | 含义 | 话术
-------|-------|-------