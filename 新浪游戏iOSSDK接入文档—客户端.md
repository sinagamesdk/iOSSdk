### 新浪游戏-ios联运sdk接入文档

### 前言
```
1. 在接入之前，请确认您的应用已登记，并已获得AppKey, AppSecret，RedirectURI(授权回调页地址)，AppID(在itunesconnect创建应用后生成的唯一标识)，BundleID（com.sina.sng.xxxx）。
2. 请发送2-3个测试设备UDID给对接人员，生成相应描述文件以供开发。
（由于账号可注册设备数量有限，请提供不同型号设备。其他人员可使用TestFlight邀请测试。）
3. 由于相关方面要求，sdk提供了游戏用户协议及实名认证。
```
### 1.导入sdk文件到工程
```
1. 将sdk静态库添加到工程。
2. Target->Buid Settings->Linking 下 Other Linker Flags 项添加 -ObjC  -all_load。（避免静态库中类加载不全造成程序崩溃）
```
### 2.设置工程回调 URL Types
```
修改 info.plist 文件 URL Types 项,添加SSO回调地址,wyx+Appkey，wb+Appkey,例如:wyx3470157549，wb3470157549 如图所示:
```
![输入图片说明](https://git.oschina.net/uploads/images/2017/0912/170801_66dccebf_478034.png "在这里输入图片标题")
### 3.添加sdk依赖库
![输入图片说明](https://git.oschina.net/uploads/images/2017/0912/172427_5d502160_478034.png "D5CBB25E-A0AA-4660-9070-1B0421A4BD07.png")
### 4.在Appdelegate.m中向微博注册Appkey
```
1. 程序启动时,在代码中向微博终端注册你的Appkey,如果首次集成联运SDK,建议打开调试选项以便输出调试信息。
2. #import "SNGUnionWeiboSDK.h"
3. 添加代理SNGUnionWeiboSDKDelegate

/**
 *  向微博注册
 *  @param kAppKey 新浪游戏唯一标识
 *  @param kAppid  游戏在itunesconnect创建应用后生成的唯一标识
 *  @param kRedirectURI，授权回调页地址
 */

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    //向微博注册
    [SNGUnionWeiboSDK enableDebugMode:YES];
    [SNGUnionWeiboSDK registerApp:kAppKey
                            appid:kAppid
                      redirectURI:kRedirectURI];
    return YES;
}
```
### 5.重写Appdelegate.m中handleOpenURL和OpenURL方法，处理授权返回
```
- (void)didReceiveWeiboResponse:(WBBaseResponse *)response
{
    if ([response isKindOfClass:WBAuthorizeResponse.class])
    {      
            ／*此方法会收到授权回调认证结果*／
    }
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options NS_AVAILABLE_IOS(9_0)
{
    return [SNGUnionWeiboSDK handleOpenURL:url delegate:self];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [SNGUnionWeiboSDK handleOpenURL:url delegate:self];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [SNGUnionWeiboSDK handleOpenURL:url delegate:self];
}
```
### 6.登录，退出，绑定
```
1. 使用示例：(SNGUnionUtil.h)

/**
 *  登录(微博登录，快速登录，其他登录)
 *  @param reloadView 二次登录时是否展示登录框，YES：展示，NO：不展示
 *  @param success 登录成功返回
 *  @param failure 登录失败返回
 *  @param bindsuccess 绑定成功返回
 *  @param bindfailure 绑定失败返回
 *  @ 当用户为游客模式，在loading页展示绑定入口
 */

+ (void)sng_LoginWithReloadView:(BOOL)reloadView
                        success:(void (^)(id params))success
                        failure:(void (^)(id params, NSError *error))failure
                    bindsuccess:(void (^)(id params))bindsuccess
                    bindfailure:(void (^)(id params, NSError *error))bindfailure;

/**
 *  注销
 *
 *  @return 注销成功返回YES，注销失败返回NO。
 */
+ (BOOL)sng_LogOut;

/**
 *  游客绑定（与账号相关）
 *
 *  @param success 成功返回
 *  @param failure 失败返回
 */
+ (void)sng_BindSuccess:(void (^)(id params))success
                failure:(void (^)(id params, NSError *error))failure;

/**
 *  游戏福利绑定（与账号无关）
 *
 *  @param success 成功返回
 *  @param failure 失败返回
 */
+ (void)sng_yxBindSuccess:(void (^)(id params))success
                  failure:(void (^)(id params, NSError *error))failure;


```
### 7.使用sdk内置浏览器（论坛-非必需，客服-必需）
```
1.声明变量 @property (nonatomic,assign) NSInteger sngInterfaceOrientationMask;
2.添加相关代码：（横屏）

 //注册旋转通知
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    _sngInterfaceOrientationMask = UIInterfaceOrientationMaskLandscapeRight | UIInterfaceOrientationMaskLandscapeLeft;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeRotate:) name:@"SNGForumChangeRotate" object:nil];

    return YES;
}

//移除所有通知
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

 //内置浏览器旋转
- (void)changeRotate:(NSNotification *)noti{
    if ([noti.object isEqualToString:@"0"]) {
        _sngInterfaceOrientationMask = UIInterfaceOrientationMaskLandscapeRight | UIInterfaceOrientationMaskLandscapeLeft;
    }else{
        _sngInterfaceOrientationMask = UIInterfaceOrientationMaskAll;
    }
}

 //内置浏览器旋转
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    return _sngInterfaceOrientationMask;
}

3.使用示例：(SNGUnionUtil.h)

/**
 *  展示webview
 *
 *  @param path 地址（这里可以为客服地址，展示客服）
 */
+ (void)showWebViewWithUrl:(NSString *)path  completionHandler:(void (^)(void))completionHandler;

/**
 * webview展示论坛
 *
 * @param level 级别
 * @param gameCurrency 金币
 * @param winP 胜率
 * @param accessKey  平台接入key 必填
 * @param secretKey  平台接入secretKey 必填
 * @param completionHandler 关闭返回
 */
+ (void)showForumWithShortName:(NSString *)shortName
                         level:(NSString *)level
                  gameCurrency:(NSString *)gameCurrency
                          winP:(NSString *)winP
                     accessKey:(NSString *)accessKey
                     secretKey:(NSString *)secretKey
             completionHandler:(void (^)(void))completionHandler;
```
### 8.内购配置(支付)
```
1. 在itunessconnect账户中添加计费点，并将计费点的相关信息发送至新浪对接群，在后台备注。
需要的参数为：productid(产品id), appid(苹果生成的产品唯一id), amount(金额，单位：分)

2. 注册内购

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //注册内购
    [[SNGPayManager sharedInstance] addTransactionObserverAndVerify:YES];

    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    //重试验证内购
     [[SNGPayManager sharedInstance] sng_verifyReceipt];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    //移除内购
    [[SNGPayManager sharedInstance] removeTransactionObserver];
}

3. 使用示例：(SNGPayManager.h)

/**
 *  购买一个商品
 *
 *  @param productId  商品id
 *  @param appid  苹果自动为内购生成的id
 *  @param subject  标题
 *  @param amount  总金额（单位分）
 *  @param pt  透传参数
 */

- (void)sng_buyWithPid:(NSString *)productId
                 appid:(NSString *)appid
               subject:(NSString *)subject
                amount:(int)amount
                    pt:(NSString *)pt
               success:(SNGPayManagerSuccessBlock)success
          fetchProduct:(SNGPayManagerFetchProductBlock)fetchProduct
               failure:(SNGPayManagerFailureBlock)failure;

4. 客户端收到sdk发出的成功回调后可向游戏方服务器查询，是否增加了礼物。（双方后台同样需要对接支付，请和服务器技术同学对接）

5. 如使用其他支付类型，请同样添加sdk内置浏览器相关代码。
```
### 9.其他注意事项 
```
1. 由于iOS10的发布，原有ATS设置在iOS10上会出现https网络访问限制的问题，为了确保好的应用体验，我们需要采取如下措施
（一般选择1就可以，2在有些情况下不成功）

（1）支持http协议，暂时回退到http协议 info.plist加

    <key>NSAppTransportSecurity</key>
    <dict>
        <key>NSAllowsArbitraryLoads</key>
        <true/>
    </dict>

（2）添加指定域名为http白名单（包括m.game.weibo.cn／m2.game.weibo.cn）

    需要在每一个域名下添加NSExceptionMinimumTLSVersion这样的key，值的部分为TLSv1.0

    <key>NSAppTransportSecurity</key>
    <dict>
        <key>NSExceptionDomains</key>
        <dict>
            <key>sina.cn</key>
            <dict>
                <key>NSIncludesSubdomains</key>
                <true/>
                <key>NSThirdPartyExceptionRequiresForwardSecrecy</key>
                <false/>
            </dict>
            <key>weibo.cn</key>
            <dict>
                <key>NSIncludesSubdomains</key>
                <true/>
                <key>NSThirdPartyExceptionRequiresForwardSecrecy</key>
                <false/>
            </dict>
            <key>weibo.com</key>
            <dict>
                <key>NSIncludesSubdomains</key>
                <true/>
                <key>NSThirdPartyExceptionAllowsInsecureHTTPLoads</key>
                <true/>
                <key>NSThirdPartyExceptionRequiresForwardSecrecy</key>
                <false/>
            </dict>
            <key>sinaimg.cn</key>
            <dict>
                <key>NSIncludesSubdomains</key>
                <true/>
                <key>NSThirdPartyExceptionAllowsInsecureHTTPLoads</key>
                <true/>
                <key>NSThirdPartyExceptionRequiresForwardSecrecy</key>
                <false/>
            </dict>
            <key>sinajs.cn</key>
            <dict>
                <key>NSIncludesSubdomains</key>
                <true/>
                <key>NSThirdPartyExceptionAllowsInsecureHTTPLoads</key>
                <true/>
                <key>NSThirdPartyExceptionRequiresForwardSecrecy</key>
                <false/>
            </dict>
            <key>sina.com.cn</key>
            <dict>
                <key>NSIncludesSubdomains</key>
                <true/>
                <key>NSThirdPartyExceptionAllowsInsecureHTTPLoads</key>
                <true/>
                <key>NSThirdPartyExceptionRequiresForwardSecrecy</key>
                <false/>
            </dict>
        </dict>
    </dict>

2. 涉及到客户端跳转，设置scheme到LSApplicationQueriesSchemes

sinaweibo,sinaweibohd

3.  在iOS10之后需要在Info.plist中，添加新的字段获取权限，否则在iOS10上运行会导致崩溃。下面是一些常用的字段 。

Key	                                         权限
Privacy - Camera Usage Description	         相机
Privacy - Microphone Usage Description	         麦克风
Privacy - Photo Library Usage Description	 相册
Privacy - Contacts Usage Description	         通讯录
Privacy - Bluetooth Peripheral Usage Description 蓝牙
Privacy - Location When In Use Usage Description 定位
Privacy - Location Always Usage Description	 后台定位
Privacy - Calendars Usage Description	         日历

```